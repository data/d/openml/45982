# OpenML dataset: Weather_Saaleaue

https://www.openml.org/d/45982

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Weather measures from Saaleaue provided by the Max-Planck-Institute for Biogeochemistry

Several weather measures provided by Max-Planck-Institute for Biogeochemistry from the Weather Station on Top of the Roof of the Institute Building.

We have assembled all the files available as of 24-05-2024 on https://www.bgc-jena.mpg.de/wetter/weather_data.html

Preprocessing:

1 - Renamed column to 'Date Time' to 'date'

2 - Parsed the date with the format '%d.%m.%Y %H:%M:%S' and converted it to string with format %y-%m-%d %H:%M:%S (default from pandas).

3 - Replaced string '********' in column 'Rn (W/m**2)' to NaN and casted column to float.

4 - Replaced values of -9999 to nan.

Values of -9999 seems to indicate a problem with the measure. Besides, it seems that some measures only started to be collected later on the year of 2002.

5 - Renamed columns with characters that cannot be encoded with encoding utf8.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45982) of an [OpenML dataset](https://www.openml.org/d/45982). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45982/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45982/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45982/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

